<?php


 abstract class Conta
 {
     public $tipoDaConta;
     public $Conta;
     public $Saldo;
     public $Agencia;

     public function imprimeConta()
     {
         echo 'CONTA: '.$this-> tipoDaConta . ' Agencia: ' . $this->Agencia . ' Conta:' . $this->Conta . ' Saldo:' . $this->calcularSaldo();
     }

     public function deposito(float $valor)
     {
         $this-> Saldo = $this-> Saldo + $valor;
     }

     public function saque(float $valor)
     {
         $this-> Saldo -= $valor;
     }

     abstract public function calcularSaldo();
 }

 class Especial extends Conta
 {
     public $SaldoEspecial;
     public function __construct(string $Agencia, string $Conta, float $SaldoEspecial)
     {
         $this-> tipoDaConta = "Especial";
         $this-> Agencia = $Agencia ;
         $this-> Conta = $Conta;
         $this-> SaldoEspecial = $SaldoEspecial;
     }

       public function calcularSaldo()
       {
           return $this-> Saldo + $this-> SaldoEspecial;
       }
 }

 class Poupanca extends Conta
 {
     public $Reajuste;

     public function __construct(string $Agencia, string $Conta, float $reajuste)
     {
         $this-> tipoDaConta = "Poupança";
         $this-> Agencia = $Agencia;
         $this-> Conta = $Conta;
         $this-> Reajuste = $reajuste;
     }

       public function calcularSaldo()
       {
           return $this-> Saldo + ($this-> Saldo * $this-> Reajuste /100);
       }
 }

 $ctaPoupanca = new Poupanca('0002-7', '85588-88', 0.54);


 $ctaPoupanca-> deposito(1500);


 $ctaPoupanca-> imprimeConta();


 echo "<hr>";


 $ctaEspecial = new Especial('0055-2', '75588-42', 2300);


 $ctaEspecial-> deposito(1500);


 $ctaEspecial-> imprimeConta();
