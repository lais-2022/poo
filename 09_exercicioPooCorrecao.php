<?php

    abstract class Conta
    {
        private $tipoDaConta;
        public function getTipoDaConta(){
            return $this-> tipoDaConta;
        }

        public function setTipoDaConta (string $tipoDaConta){
            $this-> tipoDaConta = $tipoDaConta;
        }

        public $agencia;
        public $conta;
        public $saldo;

       // protected $saldo; não pode acessar atributo protegido

        public function imprimeExtrato()
        {
            echo 'CONTA: '.$this-> tipoDaConta . ' Agencia: ' . $this->agencia . ' Conta:' . $this->conta . ' Saldo:' . $this->calcularSaldo();
        }

        public function deposito (float $valor)
        {
           if ($valor > 0)
           {
               $this-> saldo +=$valor;
               echo 'Deposito efetuado com sucesso!';
           }
           else
           {
               echo ' Valor Invalido';
           }
        }

        public function saque (float $valor)
        {
            //$this-> saldo = $this->saldo + $valor;
           // $this-> saldo -= $valor;
           
           if($this-> saldo >= $valor)
           {
               $this-> saldo -= $valor;
               echo 'Saque realizado com sucesso';

           }
           else
           {
               echo 'Saldo insuficiente!!';
           }
        }

        abstract public function calcularSaldo();

    }

    class poupanca extends conta
    {
        public $reajuste;

        public function __construct (string $agencia, string $conta, float $reajuste)
        {
            $this-> setTipoDaConta  ('poupança');
            $this-> $agencia = $agencia;
            $this-> $conta = $conta;
            $this-> $reajuste = $reajuste;
        }

        public function calcularSaldo()
        {
            return $this-> saldo + ($this-> saldo * $this-> reajuste / 100);
        }
    }
   
    class especial extends conta
    {
        public $SaldoEspecial;

        public function __construct (string $agencia, string $conta, float $SaldoEspecial)
        {
            $this-> setTipoDaConta  ('SaldoEspecial');
            $this-> $agencia = $agencia;
            $this-> $conta = $conta;
            $this-> $SaldoEspecial = $SaldoEspecial;
        }

        public function calcularSaldo()
        {
            return $this-> saldo + $this-> SaldoEspecial;
        }
    }

    $ctaPoupanca = new poupanca('0002-7', '8588-88', 0.54);

    //$ctaPoupanca-> saldo = -1500;
    //não pode acessar atributo protegido

    $ctaPoupanca-> deposito (1500);
    $ctaPoupanca-> saque(3000);
    $ctaPoupanca-> imprimeExtrato();

    echo "<hr>";

    $ctaEspecial = new Especial('0055-2', '75588-42', 2300);
    $ctaEspecial-> deposito(1500);
    $ctaEspecial-> imprimeExtrato();
   
?>